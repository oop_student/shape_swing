/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontapat.shapeprojectswing;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author DELL
 */
public class RectangleFrame extends JFrame{
    JLabel lblWidth;
    JLabel lblLength;
    JTextField txtWidth;
    JTextField txtLength;
    JButton btnCalculate;
    JLabel lblResult;
public RectangleFrame() {
        super("Rectangle");
        this.init();
    }
 public void init() {
        this.setSize(500, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        
        lblWidth = new JLabel("Width:",JLabel.TRAILING);
        lblWidth.setSize(50, 20);
        lblWidth.setLocation(5, 5);
        lblWidth.setBackground(Color.WHITE);
        lblWidth.setOpaque(true);
        this.add(lblWidth);
        
        lblLength = new JLabel("Length:",JLabel.TRAILING);
        lblLength.setSize(50, 20);
        lblLength.setLocation(5, 25);
        lblLength.setBackground(Color.WHITE);
        lblLength.setOpaque(true);
        this.add(lblLength);
    
        txtWidth = new JTextField();
        txtWidth.setSize(50, 20);
        txtWidth.setLocation(60, 5);
        this.add(txtWidth);
        
        txtLength = new JTextField();
        txtLength.setSize(50, 20);
        txtLength.setLocation(60, 25);
        this.add(txtLength);
        
        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);
        
        lblResult = new JLabel("Rectangle width= ??? length=??? area= ??? perimeter= ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500, 50);
        lblResult.setLocation(0, 100);
        lblResult.setBackground(Color.ORANGE);
        lblResult.setOpaque(true);
        this.add(lblResult);
        
        
        btnCalculate.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                
                String strWidth = txtWidth.getText();
                double width = Double.parseDouble(strWidth);
                String strLength = txtLength.getText();
                double length = Double.parseDouble(strLength);
                
                Rectangle rectangle = new Rectangle (width ,length);
                lblResult.setText("Rectangle width = " + String.format("%.2f", rectangle.getWidth())
                        + " length = " + String.format("%.2f", rectangle.getLength())
                        + " area = " + String.format("%.2f", rectangle.calArea()) 
                        + " perimeter = " + String.format("%.2f", rectangle.calPerimeter()) ) ;
                } catch(Exception ex) {
                    JOptionPane.showMessageDialog(RectangleFrame.this , "Error: Please input number", "Error"
                            , JOptionPane.ERROR_MESSAGE);
                    txtWidth.setText("");
                    txtWidth.requestFocus();
                }
            }
        });  
    }
    public static void main(String[] args) {
        RectangleFrame frame = new RectangleFrame();
        frame.setVisible(true);
    }
}
