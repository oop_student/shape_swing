/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontapat.shapeprojectswing;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author DELL
 */
public class TriangleFrame extends JFrame{
    JLabel lblBase;
    JLabel lblHeight;
    JTextField txtBase;
    JTextField txtHeight;
    JButton btnCalculate;
    JLabel lblResult;
public TriangleFrame() {
        super("Rectangle");
        this.init();
    }
 public void init() {
        this.setSize(500, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        
        lblBase = new JLabel("Base:",JLabel.TRAILING);
        lblBase.setSize(50, 20);
        lblBase.setLocation(5, 5);
        lblBase.setBackground(Color.WHITE);
        lblBase.setOpaque(true);
        this.add(lblBase);
        
        lblHeight = new JLabel("Height:",JLabel.TRAILING);
        lblHeight.setSize(50, 20);
        lblHeight.setLocation(5, 25);
        lblHeight.setBackground(Color.WHITE);
        lblHeight.setOpaque(true);
        this.add(lblHeight);
    
        txtBase = new JTextField();
        txtBase.setSize(50, 20);
        txtBase.setLocation(60, 5);
        this.add(txtBase);
        
        txtHeight = new JTextField();
        txtHeight.setSize(50, 20);
        txtHeight.setLocation(60, 25);
        this.add(txtHeight);
        
        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);
        
        lblResult = new JLabel("Triangle base= ??? height=??? area= ??? perimeter= ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500, 50);
        lblResult.setLocation(0, 100);
        lblResult.setBackground(Color.GREEN);
        lblResult.setOpaque(true);
        this.add(lblResult);
        
        
        btnCalculate.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                
                String strBase = txtBase.getText();
                double base = Double.parseDouble(strBase);
                String strHeight = txtHeight.getText();
                double height = Double.parseDouble(strHeight);
                
                Triangle triangle = new Triangle (base ,height);
                lblResult.setText("Triangle base = " + String.format("%.2f", triangle.getB())
                        + " Height = " + String.format("%.2f", triangle.getH())
                        + " area = " + String.format("%.2f", triangle.calArea()) 
                        + " perimeter = " + String.format("%.2f", triangle.calPerimeter()) ) ;
                } catch(Exception ex) {
                    JOptionPane.showMessageDialog(TriangleFrame.this , "Error: Please input number", "Error"
                            , JOptionPane.ERROR_MESSAGE);
                    txtBase.setText("");
                    txtBase.requestFocus();
                }
            }
        });  
    }    
    public static void main(String[] args) {
        TriangleFrame frame = new TriangleFrame();
        frame.setVisible(true);
    }
}
