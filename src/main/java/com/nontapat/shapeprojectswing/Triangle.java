/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontapat.shapeprojectswing;

/**
 *
 * @author DELL
 */
public class Triangle extends Shape{
    private double base;
    private double height;
    public static final double half = 1.0/2;
    
    public Triangle(double b, double h) {
        super("Triangle");
        this.base = b;
        this.height = h;
    }
    
    public double calArea() {
        return half * base * height;
    }
    
    public void setB(double b) {

        this.base = b;
    }
    
    public void setH(double h) {

        this.height = h;
    }
    
    public double getB() {
        return base;
    }
    
    public double getH() {
        return height; 
    }    

    @Override
    public double calPerimeter() {
        double othSide = Math.sqrt(Math.pow(0.5 * base,2) + Math.pow(height, 2));
        return (othSide*2) + base;
    }


}
